<%@ page import="hu.alkfejl.config.RecipeSearchServlet" %><%--
  Created by IntelliJ IDEA.
  User: Tamas
  Date: 2021. 04. 22.
  Time: 20:38
  To change this template use File | Settings | File Templates.
--%>
<%@page import="java.util.List" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="hu.alkfejl.config.RecipeSearchServlet" %>


<c:if test="${requestScope.recipes == null}">
    <%
        request.getRequestDispatcher("RecipeSearchServlet").include(request, response);
    %>
</c:if>

<head>
    <title>Szakacskonyv</title>
</head>
<body>

<form action="RecipeSearchServlet" method="POST">
    Név: <label>
    <input type="text" name="name"/>
</label>
    <br>
    <input type="submit" value="Submit"/>
</form>

<div class="container">

    <table class="table">
        <thead class="thead__dark">
        <tr>
            <th scope="col">name</th>
            <th scope="col">optionalName</th>
            <th scope="col">description</th>
            <th scope="col">difficulty</th>
            <th scope="col">period</th>
            <th scope="col">category</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="recipe" items="${requestScope.recipes}">
            <tr>
                <td>${recipe.name}</td>
                <td>${recipe.optionalName}</td>
                <td>${recipe.description}</td>
                <td>${recipe.difficulty}</td>
                <td>${recipe.period}</td>
                <td>${recipe.dose}</td>
                <td>${recipe.category}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <%--    <p>${requestScope}</p>--%>
</div>
</body>
</html>