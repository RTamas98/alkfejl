package hu.alkfejl;


import hu.alkfejl.config.Start;
import hu.alkfejl.util.ConnectSQL;
import hu.alkfejl.util.Config;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    private static Stage stage;

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        Start.getInstance().initScene();
        stage.setTitle(Config.getExample().getResourceBundle().getString("application.name"));
        stage.show();
    }

    public static Stage getStage() {
        return stage;
    }

    public static void main(String[] args) {
        ConnectSQL.getInstance();
        launch(args);
    }
}
