package hu.alkfejl.util;



import hu.alkfejl.model.RawMaterial;
import hu.alkfejl.model.Stock;
import hu.alkfejl.model.Menu;
import hu.alkfejl.model.Recipe;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ConnectSQL {


	private static ConnectSQL instance = null;

	private ConnectSQL() {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		try {
			Class.forName(Config.getExample().getConfig().getString("sqlite.driver"));
			System.out.println("JDBC class loaded");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		String dbPath = Config.getExample().getConfig().getString("sqlite.db_path");
		File dbFile = new File(dbPath.replaceAll("jdbc:sqlite:",""));
		boolean dbExists = dbFile.exists();
		try (Connection connection = createConnection()) {
			System.out.println("Jdbc Connected");
			try (Statement statement = connection.createStatement()) {
				if(!dbExists) {

				statement.executeUpdate(SQL.createRawMaterialTable);
				statement.executeUpdate(SQL.createStockTable);
				statement.executeUpdate(SQL.createRecipeTable);
				statement.executeUpdate(SQL.createIngredientTable);
				statement.executeUpdate(SQL.createMenuTable);
				statement.executeUpdate(SQL.createSwitchRecipeTable);

				statement.executeUpdate(SQL.insertMaterial("tojas","DARAB"));
				statement.executeUpdate(SQL.insertMaterial("olaj","LITER"));
				statement.executeUpdate(SQL.insertMaterial("csirke mell","KILOGRAMM"));
				statement.executeUpdate(SQL.insertMaterial("vegetta","CSIPET"));
				statement.executeUpdate(SQL.insertMaterial("bors","CSIPET"));
				statement.executeUpdate(SQL.insertMaterial("zsemlemorzsa","DEKAGRAMM"));
				statement.executeUpdate(SQL.insertMaterial("tej","DECILITER"));
				statement.executeUpdate(SQL.insertMaterial("salata","DARAB"));
				statement.executeUpdate(SQL.insertMaterial("brokkoli","DARAB"));
				statement.executeUpdate(SQL.insertMaterial("fokhagyma por","DEKAGRAMM"));
				statement.executeUpdate(SQL.insertMaterial("repa","DEKAGRAMM"));
				statement.executeUpdate(SQL.insertMaterial("krumpli","DEKAGRAMM"));
				statement.executeUpdate(SQL.insertMaterial("csipetke","DEKAGRAMM"));
				statement.executeUpdate(SQL.insertMaterial("piros paprika","EVO_KANAL"));
				statement.executeUpdate(SQL.insertMaterial("marha labszar","KILOGRAMM"));

				statement.executeUpdate(SQL.insertDefaultStock(5,1));
				statement.executeUpdate(SQL.insertDefaultStock(6,2));
				statement.executeUpdate(SQL.insertDefaultStock(8,3));
				statement.executeUpdate(SQL.insertDefaultStock(5,4));
				statement.executeUpdate(SQL.insertDefaultStock(12,5));
				statement.executeUpdate(SQL.insertDefaultStock(5,6));
				statement.executeUpdate(SQL.insertDefaultStock(14,7));
				statement.executeUpdate(SQL.insertDefaultStock(16,8));
				statement.executeUpdate(SQL.insertDefaultStock(13,9));
				statement.executeUpdate(SQL.insertDefaultStock(16,10));

				statement.executeUpdate(SQL.insertDefaultRecipe("rantott hus","becsi szelet","A legjobb magyar kajat ne hagyd ki!","MEDIUM",30,2,"MAIN_FOOD"));
				statement.executeUpdate(SQL.insertDefaultRecipe("salata","healthy-food","A dietadban o segit","EASY",5,4,"SALAD"));
				statement.executeUpdate(SQL.insertDefaultRecipe("gulyas leves","hungarian gulash","Az orszag kedvence!","HARD",100,6,"SOUP"));
				statement.executeUpdate(SQL.insertDefaultRecipe("sult krumpli","hasabburgonya","Ugyis dagadt vagy mar, ez meg belefer.","EASY",10,6,"SIDE_DISH"));

				statement.executeUpdate(SQL.insertDefaultIngredient(1,3,1));
				statement.executeUpdate(SQL.insertDefaultIngredient(2,2,1));
				statement.executeUpdate(SQL.insertDefaultIngredient(6,1,1));
				statement.executeUpdate(SQL.insertDefaultIngredient(3,4,1));
				statement.executeUpdate(SQL.insertDefaultIngredient(20,6,1));
				statement.executeUpdate(SQL.insertDefaultIngredient(5,5,1));
				statement.executeUpdate(SQL.insertDefaultIngredient(10,8,2));
				statement.executeUpdate(SQL.insertDefaultIngredient(5,15,3));
				statement.executeUpdate(SQL.insertDefaultIngredient(3,14,3));
				statement.executeUpdate(SQL.insertDefaultIngredient(40,13,3));
				statement.executeUpdate(SQL.insertDefaultIngredient(40,12,3));
				statement.executeUpdate(SQL.insertDefaultIngredient(100,11,3));
				statement.executeUpdate(SQL.insertDefaultIngredient(10,10,3));
				statement.executeUpdate(SQL.insertDefaultIngredient(5,4,3));
				statement.executeUpdate(SQL.insertDefaultIngredient(5,5,3));
				statement.executeUpdate(SQL.insertDefaultIngredient(1,12,4));
				statement.executeUpdate(SQL.insertDefaultIngredient(3,2,4));

				statement.executeUpdate(SQL.insertDefaultMenu("Becsi kulonlegesseg",30,2));
				statement.executeUpdate(SQL.insertDefaultMenu("Legegeszsegesebb etelunk",5,4));
				statement.executeUpdate(SQL.insertDefaultMenu("Magyar kulonlegesseg",100,6));
				statement.executeUpdate(SQL.insertDefaultMenu("Top koret",10,6));
				statement.executeUpdate(SQL.insertDefaultKapcsoloRecept(1,1));
				statement.executeUpdate(SQL.insertDefaultKapcsoloRecept(2,2));
				statement.executeUpdate(SQL.insertDefaultKapcsoloRecept(3,3));
				statement.executeUpdate(SQL.insertDefaultKapcsoloRecept(4,4));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static ConnectSQL getInstance() {
		if (instance == null) {
			return instance = new ConnectSQL();
		}
		return instance;
	}


	private Connection createConnection() throws SQLException {
		return DriverManager.getConnection(Config.getExample().getConfig().getString("sqlite.db_path"));
	}

	private Integer selectLastInsertedRowId() {
		int rowId = 0;
		try (Connection connection = createConnection()) {
			System.out.println("Last Insterted Connected");
			try (Statement statement = connection.createStatement()) {
				System.out.println("Select last inserted row Id");
				ResultSet rs = statement.executeQuery(SQL.lastInsertedRowId + "recipe");
				while (rs.next()) {
					rowId = rs.getInt("lastid");
				}
				System.out.println("RowId: " + rowId);
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return rowId;
	}

	public List<RawMaterial> selectAllRawMaterial() {
		List<RawMaterial> raw_materials = new ArrayList<>();

		try (Connection connection = createConnection()) {
			System.out.println("All Raw Material Connected");
			try (Statement statement = connection.createStatement()) {
				System.out.println("Select all raw material");
				ResultSet rs = statement.executeQuery(SQL.selectAllRawMaterial);
				while (rs.next()) {
					RawMaterial raw_material = new RawMaterial.Builder()
							.setId(rs.getInt("id"))
							.setNev(rs.getString("name"))
							.setMertekegyseg(rs.getString("unit"))
							.setTovabbiMertekegyseg(rs.getString("other_unit"))
							.build();
					raw_materials.add(raw_material);
				}
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return raw_materials;
	}

	public RawMaterial selectRawMaterialById(Integer id) {
		RawMaterial raw_material = null;
		try (Connection connection = createConnection()) {
			System.out.println("Raw Material By Id Connected");
			try (PreparedStatement statement = connection.prepareStatement(SQL.selectRawMaterialById)) {
				System.out.println("Select raw material by ID");
				statement.setInt(1, id);
				ResultSet rs = statement.executeQuery();
				while (rs.next()) {
					raw_material = new RawMaterial.Builder()
							.setId(rs.getInt("id"))
							.setNev(rs.getString("name"))
							.setMertekegyseg(rs.getString("unit"))
							.setTovabbiMertekegyseg(rs.getString("other_unit"))
							.build();
				}
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return raw_material;
	}

	public void insertRawMaterial(RawMaterial raw_material) {
		try (Connection connection = createConnection()) {
			System.out.println("Insert Raw Material Connected");
			try (PreparedStatement statement = connection.prepareStatement(SQL.insertRawMaterial)) {
				System.out.println("Insert raw material");
				statement.setString(1, raw_material.getNev());
				statement.setString(2, raw_material.getMertekegyseg().toString());
				statement.setString(3, raw_material.getTovabbiMertekegyseg());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Stock> selectAllStock() {
		List<Stock> stocks = new ArrayList<>();

		try (Connection connection = createConnection()) {
			System.out.println("All Stock Connected");
			try (Statement statement = connection.createStatement()) {
				System.out.println("Select all stock");
				ResultSet rs = statement.executeQuery(SQL.selectAllStock);
				while (rs.next()) {
					Stock stock = new Stock.Builder()
							.setId(rs.getInt("id"))
							.setCount(rs.getInt("count"))
							.setRaw_material(selectRawMaterialById(rs.getInt("rawMaterial")))
							.build();
					stocks.add(stock);
				}
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return stocks;
	}

	private int runStockQuery(Connection connection, Stock stock, String query) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, stock.getCount());
		statement.setInt(2, stock.getRaw_material().getId());
		return statement.executeUpdate();
	}

	public void insertStock(Stock stock) {
		try (Connection connection = createConnection()) {
			if (runStockQuery(connection, stock, SQL.updateStockCount) == 0) {
				runStockQuery(connection, stock, SQL.insertStock);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}


//		Connection connection = null;
//		try {
//			connection = createConnection();
//		} catch (SQLException throwables) {
//			throwables.printStackTrace();
//		}
//		try (PreparedStatement statement2 = connection.prepareStatement(SQL.updateStockCount)) {
//			statement2.setInt(1,stock.getCount());
//			statement2.setInt(2,stock.getRaw_material().getId());
//			statement2.executeUpdate();
//		}
//		catch (SQLException e) {
//			try (PreparedStatement statement = connection.prepareStatement(SQL.insertStock)) {
//				System.out.println("Insert stock");
//				statement.setInt(1, stock.getCount());
//				statement.setInt(2, stock.getRaw_material().getId());
//				statement.executeUpdate();
//			} catch (SQLException throwables) {
//				throwables.printStackTrace();
//			}
//			e.printStackTrace();
//		}
//		finally {
//			try {
//				connection.close();
//			} catch (SQLException throwables) {
//				throwables.printStackTrace();
//			}
//		}
	}


	public List<Stock> selectAllIngredient() {
		List<Stock> hozzavalok = new ArrayList<>();

		try (Connection connection = createConnection()) {
			System.out.println("Select all Ingredient Connected");
			try (Statement statement = connection.createStatement()) {
				System.out.println("Select all ingredient");
				ResultSet rs = statement.executeQuery(SQL.selectAllIngredient);
				while (rs.next()) {
					Stock hozzavalo = new Stock.Builder()
							.setId(rs.getInt("id"))
							.setCount(rs.getInt("count"))
							.setRaw_material(selectRawMaterialById(rs.getInt("raw_material")))
							.build();
					hozzavalok.add(hozzavalo);
				}
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return hozzavalok;
	}

	public List<Stock> selectIngredientByReceptId(Integer id) {
		List<Stock> ingedients = new ArrayList<>();

		try (Connection connection = createConnection()) {
			System.out.println("Ingredient By Recept Id Connected");
			try (PreparedStatement statement = connection.prepareStatement(SQL.selectAllIngredientByRecipeId)) {
				System.out.println("Select ingredient by recept ID");
				statement.setInt(1, id);
				ResultSet rs = statement.executeQuery();
				while (rs.next()) {
					Stock ingedient = new Stock.Builder()
							.setId(rs.getInt("id"))
							.setCount(rs.getInt("count"))
							.setRaw_material(selectRawMaterialById(rs.getInt("rawMaterial")))
							.build();
					ingedients.add(ingedient);
				}
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ingedients;
	}

	public void insertIngredient(Stock ingedient) {
		try (Connection connection = createConnection()) {
			System.out.println("Insert Ingredient Connected");
			try (PreparedStatement statement = connection.prepareStatement(SQL.insertIngredient)) {
				System.out.println("Insert ingredient");
				statement.setInt(1, ingedient.getCount());
				statement.setInt(2, ingedient.getRaw_material().getId());
				statement.setInt(3, selectLastInsertedRowId());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Recipe> selectAllRecipe() {
		List<Recipe> recipes = new ArrayList<>();

		try (Connection connection = createConnection()) {
			System.out.println("Select All Recipe Connected");
			try (Statement statement = connection.createStatement()) {
				System.out.println("Select all recipe");
				ResultSet rs = statement.executeQuery(SQL.selectAllRecipe);
				while (rs.next()) {
					Recipe recipe = new Recipe.Builder()
							.setId(rs.getInt("id"))
							.setName(rs.getString("name"))
							.setOptionalName(rs.getString("optional_name"))
							.setDescription(rs.getString("description"))
							.setNehezseg(rs.getString("difficulty"))
							.setPeriod(rs.getInt("period"))
							.setDose(rs.getInt("dose"))
							.setIngredients(selectIngredientByReceptId(rs.getInt("id")))
							.setKategoria(rs.getString("category"))
							.build();
					recipes.add(recipe);
				}
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return recipes;
	}

	private Recipe selectRecipetById(Integer id) {
		Recipe recipe = null;
		try (Connection connection = createConnection()) {
			System.out.println("Select Recipe by IdConnected");
			try (PreparedStatement statement = connection.prepareStatement(SQL.selectAllRecipeById)) {
				System.out.println("Select recipe by ID");
				statement.setInt(1, id);
				ResultSet rs = statement.executeQuery();
				while (rs.next()) {
					recipe = new Recipe.Builder()
							.setId(rs.getInt("id"))
							.setName(rs.getString("name"))
							.setOptionalName(rs.getString("optional_name"))
							.setDescription(rs.getString("description"))
							.setNehezseg(rs.getString("difficulty"))
							.setPeriod(rs.getInt("period"))
							.setDose(rs.getInt("dose"))
							.setIngredients(selectIngredientByReceptId(rs.getInt("id")))
							.setKategoria(rs.getString("category"))
							.build();
				}
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return recipe;
	}

	public void insertRecipe(Recipe recipe) {
		try (Connection connection = createConnection()) {
			System.out.println("Insert Recipe Connected");
			try (PreparedStatement statement = connection.prepareStatement(SQL.insertRecipe)) {
				System.out.println("Insert recipe");
				statement.setString(1, recipe.getName());
				statement.setString(2, recipe.getOptionalName());
				statement.setString(3, recipe.getDescription());
				statement.setString(4, recipe.getDifficulty().toString());
				statement.setInt(5, recipe.getPeriod());
				statement.setInt(6, recipe.getDose());
				statement.setString(7, recipe.getCategory().toString());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Menu> selectAllMenu() {
		List<Menu> menus = new ArrayList<>();

		try (Connection connection = createConnection()) {
			System.out.println("Select All Menu Connected");
			try (Statement statement = connection.createStatement()) {
				System.out.println("Select all menu");
				ResultSet rs = statement.executeQuery(SQL.selectAllMenu);
				while (rs.next()) {
					Menu menu = new Menu.Builder()
							.setId(rs.getInt("id"))
							.setName(rs.getString("name"))
							.setCooking_time(rs.getInt("cooking_time"))
							.setEnoughness(rs.getInt("enoughness"))
							.setFoods(selectSwitchRecipeByMenuId(rs.getInt("id")))
							.build();
					menus.add(menu);
				}
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return menus;
	}

	public List<Recipe> selectSwitchRecipeByMenuId(Integer id) {
		List<Recipe> recipes = new ArrayList<>();

		try (Connection connection = createConnection()) {
			System.out.println("Select Switch Recept By Menu Id Connected");
			try (PreparedStatement statement = connection.prepareStatement(SQL.selectSwitchRecipeByMenuId)) {
				System.out.println("Select recipe by menu ID");
				statement.setInt(1, id);
				ResultSet rs = statement.executeQuery();
				while (rs.next()) {
					recipes.add(selectRecipetById(rs.getInt("recipe_id")));
				}
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return recipes;
	}

	public int insertMenu(Menu menu) {
		try (Connection connection = createConnection()) {
			System.out.println("Connected");
			try (PreparedStatement statement = connection.prepareStatement(SQL.insertMenu)) {
				System.out.println("Insert menu");
				statement.setString(1, menu.getName());
				statement.setInt(2, menu.getCooking_time());
				statement.setInt(3, menu.getEnoughness());
				statement.executeUpdate();

				return selectLastInsertedRowId();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;
	}

	public void insertSwitchRecipe(Integer menuId, Integer recipeId) {
		try (Connection connection = createConnection()) {
			System.out.println("Connected");
			try (PreparedStatement statement = connection.prepareStatement(SQL.insertSwitchRecipe)) {
				System.out.println("Insert switch recipe");
				statement.setInt(1, menuId);
				statement.setInt(2, recipeId);
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
