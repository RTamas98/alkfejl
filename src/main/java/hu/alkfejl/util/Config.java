package hu.alkfejl.util;

import java.util.ResourceBundle;

public class Config {

	private static Config example = null;
	private final ResourceBundle stringResourceBundle;
	private final ResourceBundle configResourceBundle;

	public Config() {
		this.stringResourceBundle = ResourceBundle.getBundle("settings");
		this.configResourceBundle = ResourceBundle.getBundle("config");
	}

	public static Config getExample() {
		return (example == null) ? example = new Config() : example;
	}

	public ResourceBundle getResourceBundle() {
		return stringResourceBundle;
	}

	public ResourceBundle getConfig() {
		return configResourceBundle;
	}
}
