package hu.alkfejl.util;

public interface SQL {

	String createRawMaterialTable = "create table rawMaterial (id integer PRIMARY KEY autoincrement, name text, unit text, other_unit text)";
	String createStockTable = "create table stock (id integer PRIMARY KEY autoincrement, count integer, rawMaterial integer)";
	String createRecipeTable = "create table recipe (id integer PRIMARY KEY autoincrement, name text, optional_name text, pics blob, description text, difficulty text, period integer, dose integer, category text)";
	String createIngredientTable = "create table ingredient (id integer PRIMARY KEY autoincrement, count integer, rawMaterial integer, recipe_id integer)";
	String createMenuTable = "create table menu (id integer PRIMARY KEY autoincrement, name text, cooking_time integer, enoughness integer)";
	String createSwitchRecipeTable = "create table switch_recipe (menu_id integer, recipe_id integer)";

	// region INSERT

	static String insertMaterial(String name, String unit) {
		return "INSERT INTO rawMaterial (name, unit) VALUES ('" + name + "','" + unit + "')";
	}

	static String insertDefaultStock(Integer count, Integer rawMaterial) {
		return "INSERT INTO stock (count, rawMaterial) VALUES ('" + count + "','" + rawMaterial + "')";
	}

	static String insertDefaultRecipe(String name, String optional_name,String description, String difficulty, Integer period, Integer dose,String category) {
		return "INSERT INTO recipe (name,optional_name,description,difficulty,period,dose,category) VALUES ('" + name + "','" + optional_name +  "','"  + description + "','" + difficulty + "','" + period + "','" + dose + "','" + category +"')";
	}

	static String insertDefaultIngredient(Integer count, Integer rawMaterial,Integer recipe_id) {
		return "INSERT INTO ingredient (count, rawMaterial,recipe_id) VALUES ('" + count + "','" + rawMaterial + "','" + recipe_id + "')";
	}

	static String insertDefaultMenu(String name, Integer cooking_time,Integer enoughness) {
		return "INSERT INTO menu (name, cooking_time, enoughness) VALUES ('" + name + "','" + cooking_time + "','" + enoughness + "')";
	}

	static String insertDefaultKapcsoloRecept(Integer menu_id,Integer recipe_id) {
		return "INSERT INTO switch_recipe (menu_id, recipe_id) VALUES ('" + menu_id + "','" + recipe_id + "')";
	}

	// endregion


	String lastInsertedRowId = "select max(id) as lastid from ";

	String selectAllRawMaterial = "select * from rawMaterial";
	String selectRawMaterialById = "select * from rawMaterial where id = ?";
	String selectRawMaterialByName = "select * from rawMaterial where name = '?'";
	String insertRawMaterial = "insert into rawMaterial(name, unit, other_unit) values (?,?,?)";
	String updateStockCount = "update stock set count = count + ? where rawMaterial = ?";

	String selectAllStock = "select * from stock";
	String insertStock = "insert into stock (count, rawMaterial) values (?,?)";

	String selectAllIngredient= "select * from ingredient";
	String insertIngredient = "insert into ingredient (count, rawMaterial, recipe_id) values (?,?,?)";
	String selectAllIngredientByRecipeId = "select * from ingredient where recipe_id = ?";

	String selectAllRecipe = "select * from recipe";
	String selectAllRecipeById = "select * from recipe where id = ?";
	String insertRecipe = "insert into recipe (name, optional_name, description, difficulty, period, dose, category) values (?,?,?,?,?,?,?)";

	String selectAllMenu = "select * from menu";
	String insertMenu = "insert into menu (name, cooking_time, enoughness) values (?,?,?)";
	String insertSwitchRecipe = "insert into switch_recipe values (?,?)";
	String selectSwitchRecipeByMenuId = "select recipe_id from switch_recipe where menu_id = ?";
}
