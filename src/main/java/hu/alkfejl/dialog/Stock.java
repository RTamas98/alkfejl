package hu.alkfejl.dialog;


import hu.alkfejl.config.StockDialogController;
import hu.alkfejl.util.Config;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

import java.io.IOException;
import java.util.Optional;

public class Stock {

	public static Optional<hu.alkfejl.model.Stock> show() {
		final Dialog<hu.alkfejl.model.Stock> dialog = new Dialog<>();
		dialog.setTitle(Config.getExample().getResourceBundle().getString("application.name"));
		dialog.setHeaderText(Config.getExample().getResourceBundle().getString("dialog.shopping"));

		FXMLLoader loader = new FXMLLoader(Stock.class.getResource("/fxml/stock.fxml"));
		loader.setResources(Config.getExample().getResourceBundle());
		Parent root = null;
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		StockDialogController controller = loader.getController();
		dialog.getDialogPane().setContent(root);

		final ButtonType buttonTypeOk = new ButtonType(Config.getExample().getResourceBundle().getString("dialog.add"), ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);

		dialog.setResultConverter(buttonType -> {
			if (buttonType == buttonTypeOk) {
				if (!controller.quantityTextField.getText().isEmpty()) {
					return new hu.alkfejl.model.Stock.Builder()
							.setCount(Integer.parseInt(controller.quantityTextField.getText()))
							.setRaw_material(controller.rawMaterialComboBox.getValue())
							.build();
				}
			}
			return null;
		});

		Optional<hu.alkfejl.model.Stock> result = dialog.showAndWait();
		result.ifPresent(stock -> System.out.println("Stock: " + stock));
		return result;
	}
}
