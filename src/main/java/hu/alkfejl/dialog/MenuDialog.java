package hu.alkfejl.dialog;


import hu.alkfejl.config.MenuDialogController;
import hu.alkfejl.model.Menu;
import hu.alkfejl.util.Config;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

import java.io.IOException;
import java.util.Optional;

public class MenuDialog {

	public static Optional<Menu> show() {
		final Dialog<Menu> dialog = new Dialog<>();
		dialog.setTitle(Config.getExample().getResourceBundle().getString("application.name"));
		dialog.setHeaderText(Config.getExample().getResourceBundle().getString("dialog.add_menu"));

		FXMLLoader loader = new FXMLLoader(RecipeDialog.class.getResource("/fxml/menu.fxml"));
		loader.setResources(Config.getExample().getResourceBundle());
		Parent root = null;
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		MenuDialogController controller = loader.getController();
		dialog.getDialogPane().setContent(root);

		final ButtonType buttonTypeOk = new ButtonType(Config.getExample().getResourceBundle().getString("dialog.add"), ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);

		dialog.setResultConverter(buttonType -> {
			if (buttonType == buttonTypeOk) {
				Menu menu = new Menu.Builder()
						.setName(controller.nameTextField.getText())
						.setCooking_time(Integer.parseInt(controller.cookingTimeNumberField.getText()))
						.setEnoughness(Integer.parseInt(controller.enoughnessNumberField.getText()))
						.setFoods(controller.chosenrecipt)
						.build();
				return menu;
			}
			return null;
		});

		Optional<Menu> result = dialog.showAndWait();
		result.ifPresent(menu -> System.out.println("Menu: " + menu));
		return result;
	}
}
