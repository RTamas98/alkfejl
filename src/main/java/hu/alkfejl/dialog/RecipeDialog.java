package hu.alkfejl.dialog;


import hu.alkfejl.config.RecipeDialogController;
import hu.alkfejl.model.Recipe;
import hu.alkfejl.util.Config;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

import java.io.IOException;
import java.util.Optional;

public class RecipeDialog {

	public static Optional<Recipe> show() {
		final Dialog<Recipe> dialog = new Dialog<>();
		dialog.setTitle(Config.getExample().getResourceBundle().getString("application.name"));
		dialog.setHeaderText(Config.getExample().getResourceBundle().getString("dialog.add_recipe"));

		FXMLLoader loader = new FXMLLoader(RecipeDialog.class.getResource("/fxml/recipe.fxml"));
		loader.setResources(Config.getExample().getResourceBundle());
		Parent root = null;
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		RecipeDialogController controller = loader.getController();
		dialog.getDialogPane().setContent(root);

		final ButtonType buttonTypeOk = new ButtonType(Config.getExample().getResourceBundle().getString("dialog.add"), ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);

		dialog.setResultConverter(buttonType -> {
			if (buttonType == buttonTypeOk) {
				return new Recipe.Builder()
						.setName(controller.nameTextField.getText())
						.setOptionalName(controller.opcionalNameTextField.getText())
						.setPics(controller.pics)
						.setDescription(controller.descriptionTextArea.getText())
						.setDifficulty(controller.difficultyComboBox.getValue())
						.setPeriod(Integer.parseInt(controller.periodNumberField.getText()))
						.setDose(Integer.parseInt(controller.doseNumberField.getText()))
						.setIngredients(controller.ingredients)
						.setCategory(controller.categoryComboBox.getValue())
						.build();
			}
			return null;
		});

		Optional<Recipe> result = dialog.showAndWait();
		result.ifPresent(recipe -> System.out.println("Recipe: " + recipe));
		return result;
	}
}
