package hu.alkfejl.dialog;


import hu.alkfejl.config.RawMaterialDialogController;
import hu.alkfejl.model.RawMaterial;
import hu.alkfejl.util.Config;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class RawMaterialDialog {

	public static Optional<RawMaterial> showAddDialog() {
		final Dialog<RawMaterial> dialog = new Dialog<>();
		dialog.setTitle(Config.getExample().getResourceBundle().getString("application.name"));
		dialog.setHeaderText(Config.getExample().getResourceBundle().getString("dialog.add_material"));

		FXMLLoader loader = new FXMLLoader(RawMaterialDialog.class.getResource("/fxml/raw_material.fxml"));
		loader.setResources(Config.getExample().getResourceBundle());
		Parent root = null;
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		RawMaterialDialogController controller = loader.getController();
		dialog.getDialogPane().setContent(root);

		final ButtonType buttonTypeOk = new ButtonType(Config.getExample().getResourceBundle().getString("dialog.add"), ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);

		dialog.setResultConverter(buttonType -> {
			if (buttonType == buttonTypeOk) {
				if (!controller.nameTextField.getText().isEmpty() && !controller.unitComboBox.getSelectionModel().isEmpty()) {
					return new RawMaterial.Builder()
							.setNev(controller.nameTextField.getText())
							.setMertekegyseg(controller.unitComboBox.getValue())
							.setTovabbiMertekegyseg(controller.otherUnitTextField.getText())
							.build();
				}
			}
			return null;
		});

		Optional<RawMaterial> result = dialog.showAndWait();
		result.ifPresent(rawMaterial -> System.out.println("Alapanyag: " + rawMaterial));
		return result;
	}

	public static void showRemoveDialog(List<RawMaterial> alapanyagok) {
		final Dialog<RawMaterial> dialog = new Dialog<>();
		dialog.setTitle(Config.getExample().getResourceBundle().getString("application.name"));
		dialog.setHeaderText(Config.getExample().getResourceBundle().getString("dialog.remove_material"));

		final ObservableList<RawMaterial> options = FXCollections.observableArrayList(alapanyagok);
		final ComboBox<RawMaterial> rawMaterialComboBox = new ComboBox<>(options);
		dialog.getDialogPane().setContent(rawMaterialComboBox);

		final ButtonType buttonTypeOk = new ButtonType(Config.getExample().getResourceBundle().getString("dialog.remove"), ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);

		dialog.setResultConverter(buttonType -> {
			if (buttonType == buttonTypeOk) {
				RawMaterial alapanyag = rawMaterialComboBox.getSelectionModel().getSelectedItem();
				alapanyagok.remove(alapanyag);
			}
			return null;
		});
		dialog.showAndWait();
	}
}
