package hu.alkfejl.dialog;


import hu.alkfejl.config.MinRawMaterialDialogController;
import hu.alkfejl.model.Stock;
import hu.alkfejl.util.Config;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

import java.io.IOException;

public class MinRawMaterialsDialog {

	public static void show() {
		final Dialog<Stock> dialog = new Dialog<>();
		dialog.setTitle(Config.getExample().getResourceBundle().getString("application.name"));
		dialog.setHeaderText(Config.getExample().getResourceBundle().getString("dialog.min_ingredients"));
		FXMLLoader loader = new FXMLLoader(MinRawMaterialsDialog.class.getResource("/fxml/min_raw_materials.fxml"));
		loader.setResources(Config.getExample().getResourceBundle());
		Parent root = null;
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		MinRawMaterialDialogController controller = loader.getController();
		dialog.getDialogPane().setContent(root);

		final ButtonType buttonTypeOk = new ButtonType(Config.getExample().getResourceBundle().getString("dialog.ok"), ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);

		dialog.setResultConverter(buttonType -> {
			return null;
		});
		dialog.showAndWait();
	}
}
