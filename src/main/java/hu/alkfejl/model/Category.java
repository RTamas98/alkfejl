package hu.alkfejl.model;

public enum Category {
	APPETIZER,
	SOUP,
	MAIN_FOOD,
	SIDE_DISH,
	SALAD,
	DESSERT
}
