package hu.alkfejl.model;

public class RawMaterial {

	private Integer id;
	private String nev;
	private Unit mertekegyseg;
	private String tovabbiMertekegyseg;

	private RawMaterial() {
	}

	public Integer getId() {
		return id;
	}

	public String getNev() {
		return nev;
	}

	public Unit getMertekegyseg() {
		return mertekegyseg;
	}

	public String getTovabbiMertekegyseg() {
		return tovabbiMertekegyseg;
	}

	@Override
	public String toString() {
		return mertekegyseg.getName() + " " + nev + ((tovabbiMertekegyseg != null) ? " " + tovabbiMertekegyseg : "");
	}

	public static class Builder {

		private Integer id;
		private String nev;
		private Unit mertekegyseg;
		private String tovabbiMertekegyseg;

		public Builder setId(Integer id) {
			this.id = id;
			return this;
		}

		public Builder setNev(String nev) {
			this.nev = nev;
			return this;
		}

		public Builder setMertekegyseg(String mertekegyseg) {
			this.mertekegyseg = Unit.valueOf(mertekegyseg);
			return this;
		}

		public Builder setMertekegyseg(Unit mertekegyseg) {
			this.mertekegyseg = mertekegyseg;
			return this;
		}

		public Builder setTovabbiMertekegyseg(String tovabbiMertekegyseg) {
			this.tovabbiMertekegyseg = tovabbiMertekegyseg;
			return this;
		}

		public RawMaterial build() {
			RawMaterial alapanyag = new RawMaterial();
			alapanyag.id = this.id;
			alapanyag.nev = this.nev;
			alapanyag.mertekegyseg = this.mertekegyseg;
			alapanyag.tovabbiMertekegyseg = this.tovabbiMertekegyseg;
			return alapanyag;
		}
	}
}
