package hu.alkfejl.model;

public enum Difficulty {
	EASY,
	MEDIUM,
	HARD,
	PRO
}
