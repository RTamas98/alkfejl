package hu.alkfejl.model;

public class Stock {

	private Integer id;
	private Integer count;
	private RawMaterial raw_material;

	public Stock() {
	}

	public Stock(Integer count, RawMaterial raw_material) {
		this.count = count;
		this.raw_material = raw_material;
	}

	public Integer getCount() {
		return count;
	}

	public RawMaterial getRaw_material() {
		return raw_material;
	}

	@Override
	public String toString() {
		return count + " " + raw_material;
	}

	public static class Builder {

		private Integer id;
		private Integer count;
		private RawMaterial raw_material;

		public Builder setId(Integer id) {
			this.id = id;
			return this;
		}

		public Builder setCount(Integer count) {
			this.count = count;
			return this;
		}

		public Builder setRaw_material(RawMaterial raw_material) {
			this.raw_material = raw_material;
			return this;
		}

		public Stock build() {
			Stock stock = new Stock();
			stock.id = this.id;
			stock.count = this.count;
			stock.raw_material = this.raw_material;
			return stock;
		}
	}
}
