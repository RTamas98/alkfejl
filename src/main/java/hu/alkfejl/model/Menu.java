package hu.alkfejl.model;

import java.util.List;

public class Menu {

	private Integer id;
	private String name;
	private Integer cooking_time;
	private Integer enoughness;
	private List<Recipe> foods;

	private Menu() {
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Integer getCooking_time() {
		return cooking_time;
	}

	public Integer getEnoughness() {
		return enoughness;
	}

	public List<Recipe> getFoods() {
		return foods;
	}

	@Override
	public String toString() {
		return "Menu{" +
				"id=" + id +
				", nev='" + name + '\'' +
				", elkeszitesiIdo=" + cooking_time +
				", elegendo=" + enoughness +
				", etelek=" + foods +
				'}';
	}

	public static class Builder {

		private Integer id;
		private String name;
		private Integer cooking_time;
		private Integer enoughness;
		private List<Recipe> foods;

		public Builder setId(Integer id) {
			this.id = id;
			return this;
		}

		public Builder setName(String name) {
			this.name = name;
			return this;
		}

		public Builder setCooking_time(Integer cooking_time) {
			this.cooking_time = cooking_time;
			return this;
		}

		public Builder setEnoughness(Integer enoughness) {
			this.enoughness = enoughness;
			return this;
		}

		public Builder setFoods(List<Recipe> foods) {
			this.foods = foods;
			return this;
		}

		public Menu build() {
			Menu menu = new Menu();
			menu.id = this.id;
			menu.name = this.name;
			menu.cooking_time = this.cooking_time;
			menu.enoughness = this.enoughness;
			menu.foods = this.foods;
			return menu;
		}
	}
}
