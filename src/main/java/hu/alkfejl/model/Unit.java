package hu.alkfejl.model;


import hu.alkfejl.util.Config;

public enum Unit {
	DARAB("mertekegyseg.darab", 0),
	GRAMM("mertekegyseg.gramm", 0),
	DEKAGRAMM("mertekegyseg.dekagramm", 0),
	KILOGRAMM("mertekegyseg.kilogramm", 0),
	CENTILITER("mertekegyseg.centiliter", 0),
	DECILITER("mertekegyseg.deciliter", 0),
	LITER("mertekegyseg.liter", 0),
	CSIPET("mertekegyseg.csipet", 1),
	POHAR("mertekegyseg.pohar", 200),
	BOGRE("mertekegyseg.bogre", 300),
	KAVES_KANAL("mertekegyseg.kaves_kanal", 3),
	TEAS_KANAL("mertekegyseg.teas_kanal", 8),
	EVO_KANAL("mertekegyseg.evo_kanal", 13);


	private String name;
	private int change;

	Unit(String name, int change) {
		this.name = Config.getExample().getResourceBundle().getString(name);
		this.change = change;
	}

	public String getName() {
		return name;
	}

	public int getChange() {
		return change;
	}
}
