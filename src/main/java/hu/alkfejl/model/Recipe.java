package hu.alkfejl.model;

import java.io.File;
import java.util.List;

public class Recipe {

	private Integer id;
	private String name;
	private String optionalName;
	private File pics;
	private String description;
	private Difficulty difficulty;
	private Integer period;
	private Integer dose;
	private List<Stock> ingredients;
	private Category category;

	private Recipe() {
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getOptionalName() {
		return optionalName;
	}

	public File getPics() {
		return pics;
	}

	public String getDescription() {
		return description;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public Integer getPeriod() {
		return period;
	}

	public Integer getDose() {
		return dose;
	}

	public List<Stock> getIngredients() {
		return ingredients;
	}

	public Category getCategory() {
		return category;
	}

	@Override
	public String toString() {
		return 	"nev='" + name + '\'' +
				", opcionalisNev='" + optionalName + '\'' +
				", leiras='" + description + '\'' +
				", nehezseg=" + difficulty +
				", idotartam=" + period +
				", adag=" + dose +
				", hozzavalok=" + ingredients +
				", kategoria=" + category;
	}

	public static class Builder {

		private Integer id;
		private String name;
		private String optionalName;
		private File pics;
		private String description;
		private Difficulty difficulty;
		private Integer period;
		private Integer dose;
		private List<Stock> ingredients;
		private Category category;

		public Builder setId(Integer id) {
			this.id = id;
			return this;
		}

		public Builder setName(String name) {
			this.name = name;
			return this;
		}

		public Builder setOptionalName(String optionalName) {
			this.optionalName = optionalName;
			return this;
		}

		public Builder setPics(File pics) {
			this.pics = pics;
			return this;
		}

		public Builder setDescription(String description) {
			this.description = description;
			return this;
		}

		public Builder setDifficulty(Difficulty difficulty) {
			this.difficulty = difficulty;
			return this;
		}

		public Builder setNehezseg(String nehezseg) {
			this.difficulty = Difficulty.valueOf(nehezseg);
			return this;
		}

		public Builder setPeriod(Integer period) {
			this.period = period;
			return this;
		}

		public Builder setDose(Integer dose) {
			this.dose = dose;
			return this;
		}

		public Builder setIngredients(List<Stock> ingredients) {
			this.ingredients = ingredients;
			return this;
		}

		public Builder setCategory(Category category) {
			this.category = category;
			return this;
		}

		public Builder setKategoria(String kategoria) {
			this.category = Category.valueOf(kategoria);
			return this;
		}

		public Recipe build() {
			Recipe recept = new Recipe();
			recept.id = this.id;
			recept.name = this.name;
			recept.optionalName = this.optionalName;
			recept.pics = this.pics;
			recept.description = this.description;
			recept.difficulty = this.difficulty;
			recept.period = this.period;
			recept.dose = this.dose;
			recept.ingredients = this.ingredients;
			recept.category = this.category;
			return recept;
		}
	}
}
