package hu.alkfejl.config;


import hu.alkfejl.model.RawMaterial;
import hu.alkfejl.model.Category;
import hu.alkfejl.model.Stock;
import hu.alkfejl.model.Difficulty;
import hu.alkfejl.util.Regex;
import hu.alkfejl.util.Config;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class RecipeDialogController implements Initializable {

	@FXML
	public GridPane root;
	public TextField nameTextField;
	public TextField opcionalNameTextField;
	public ImageView picsImageView;
	public TextArea descriptionTextArea;
	public ComboBox<Difficulty> difficultyComboBox;
	public Regex periodNumberField;
	public Regex doseNumberField;
	public TableView<Stock> ingredientsTableView;
	public TableColumn<Stock, Integer> quantityTableColumn;
	public TableColumn<Stock, String> rawMaterialTableColumn;
	public ComboBox<Category> categoryComboBox;

	public File pics;

	private final List<RawMaterial> rawMaterials = MainController.getRawMaterials();
	public final List<Stock> ingredients = new ArrayList<>();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		difficultyComboBox.setItems(FXCollections.observableArrayList(Difficulty.values()));
		categoryComboBox.setItems(FXCollections.observableArrayList(Category.values()));

		quantityTableColumn.setCellValueFactory(new PropertyValueFactory<>("count"));
		rawMaterialTableColumn.setCellValueFactory(new PropertyValueFactory<>("raw_material"));
		ingredientsTableView.setItems(FXCollections.observableArrayList(ingredients));
	}

	public void picsUpload(ActionEvent actionEvent) {
		final FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter(Config.getExample().getResourceBundle().getString("other.images"), "*.jpg", "*.jpeg", "*.png")
		);
		pics = fileChooser.showOpenDialog(root.getScene().getWindow());
		if (pics != null) {
			Image image = new Image(pics.toURI().toString());
			picsImageView.setImage(image);
		}
	}

	public void addNew(ActionEvent actionEvent) {
		final Dialog<Stock> dialog = new Dialog<>();
		dialog.setTitle(Config.getExample().getResourceBundle().getString("application.name"));

		FXMLLoader loader = new FXMLLoader(hu.alkfejl.dialog.Stock.class.getResource("/fxml/stock.fxml"));
		loader.setResources(Config.getExample().getResourceBundle());
		Parent root = null;
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		StockDialogController controller = loader.getController();
		dialog.getDialogPane().setContent(root);

		final ButtonType buttonTypeOk = new ButtonType(Config.getExample().getResourceBundle().getString("dialog.add"), ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);

		dialog.setResultConverter(buttonType -> {
			if (buttonType == buttonTypeOk) {
				if (!controller.quantityTextField.getText().isEmpty()) {
					Stock stock = new Stock.Builder()
							.setCount(Integer.parseInt(controller.quantityTextField.getText()))
							.setRaw_material(controller.rawMaterialComboBox.getValue())
							.build();
					ingredients.add(stock);
					return stock;
				}
			}
			return null;
		});

		Optional<Stock> result = dialog.showAndWait();
		if (result.isPresent()) {
			System.out.println("Result: " + result.get());
			ingredientsTableView.setItems(FXCollections.observableArrayList(ingredients));
		}
	}

	public void removeSelected(ActionEvent actionEvent) {
		Stock selectedItem = ingredientsTableView.getSelectionModel().getSelectedItem();
		ingredients.remove(selectedItem);
		ingredientsTableView.setItems(FXCollections.observableArrayList(ingredients));
	}
}
