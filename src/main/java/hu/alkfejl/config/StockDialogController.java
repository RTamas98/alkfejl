package hu.alkfejl.config;


import hu.alkfejl.model.RawMaterial;
import hu.alkfejl.util.Regex;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class StockDialogController implements Initializable {

	@FXML
	public Regex quantityTextField;
	public ComboBox<RawMaterial> rawMaterialComboBox;

	private final List<RawMaterial> rawMaterials = MainController.getRawMaterials();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		final ObservableList<RawMaterial> options = FXCollections.observableArrayList(rawMaterials);
		rawMaterialComboBox.setItems(options);
	}
}
