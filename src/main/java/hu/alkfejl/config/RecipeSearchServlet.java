package hu.alkfejl.config;

import hu.alkfejl.model.Recipe;
import hu.alkfejl.util.ConnectSQL;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;


@WebServlet("/RecipeSearchServlet")
public class RecipeSearchServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");

        List<Recipe> recipes = ConnectSQL.getInstance().selectAllRecipe().stream().filter(recipe -> recipe.getName().startsWith(name)).collect(Collectors.toList());
        req.setAttribute("recipes",recipes);

        req.getRequestDispatcher("index.jsp").forward(req,resp);

    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        String name = request.getParameter("name");

        List<Recipe> recipes = ConnectSQL.getInstance().selectAllRecipe();
        request.setAttribute("recipes",recipes);
        System.out.println(recipes);
//        System.out.println("recipes = " + recipes);
//        response.getWriter().append(recipes.toString());
//        PrintWriter out = response.getWriter();


//        for (Recipe recipe : recipes) {
//            if (recipe.getName().contains(name)) {
//                out.append(recipe.toString());
//            }
//        }
    }
//


//        ArrayList<String[]> paramValues = new ArrayList<>(request.getParameterMap().values());
//        ArrayList<Match> matches = new ArrayList<>(dao.findAllIf(paramValues.get(0)[0], paramValues.get(1)[0], paramValues.get(2)[0]));
//        StringBuilder matchesData = new StringBuilder();
//        for(Match match : matches){
//            matchesData.append(match.toString());
//        }
//        response.getWriter().append(matchesData.toString());
//    }
}
