package hu.alkfejl.config;


import hu.alkfejl.dialog.*;
import hu.alkfejl.model.RawMaterial;
import hu.alkfejl.model.Stock;
import hu.alkfejl.model.Menu;
import hu.alkfejl.model.Recipe;
import hu.alkfejl.util.ConnectSQL;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainController implements Initializable {

	@FXML

	public TableView<Menu> menusTableView;
	public TableColumn<Menu, String> menuNameTableColumn;
	public TableColumn<Menu, Integer> menuEnoughnessTableColumn;
	public TableColumn<Menu, Integer> menuCookingTimeTableColumn;
	public TableColumn<Menu, List<Recipe>> menuFoodsTableColumn;

	private static final List<RawMaterial> rawMaterials = new ArrayList<>();
	private static final List<Stock> stocks = new ArrayList<>();
	private static final List<Recipe> recipes = new ArrayList<>();
	private static final List<Menu> menus = new ArrayList<>();

	public TableView<Recipe> recipesTableView;
	public TableColumn<Recipe, String> nameTableColumn;
	public TableColumn<Recipe, String> categoryTableColumn;
	public TableColumn<Recipe, String> difficultyTableColumn;
	public TableColumn<Recipe, List<Stock>> ingredientTableColumn;
	
	public TableView<Stock> stockTableView;
	public TableColumn<Stock, Integer> quantityTableColumn;
	public TableColumn<Stock, RawMaterial> rawMaterialTableColumn;

	public static List<RawMaterial> getRawMaterials() {
		return rawMaterials;
	}

	public static List<Recipe> getRecipes() {
		return recipes;
	}

	public void initialize(URL location, ResourceBundle resources) {
		initAdd();
		initMenus();
		initRecipes();
		initStock();
	}

	private void initAdd(){
		rawMaterials.addAll(ConnectSQL.getInstance().selectAllRawMaterial());
		stocks.addAll(ConnectSQL.getInstance().selectAllStock());
		recipes.addAll(ConnectSQL.getInstance().selectAllRecipe());
		menus.addAll(ConnectSQL.getInstance().selectAllMenu());
	}

	private void initMenus(){
		menuNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		menuEnoughnessTableColumn.setCellValueFactory(new PropertyValueFactory<>("enoughness"));
		menuCookingTimeTableColumn.setCellValueFactory(new PropertyValueFactory<>("cooking_time"));
		menuFoodsTableColumn.setCellValueFactory(new PropertyValueFactory<>("foods"));
		menusTableView.setItems(FXCollections.observableArrayList(menus));
	}



	private void initRecipes(){
		nameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		categoryTableColumn.setCellValueFactory(new PropertyValueFactory<>("category"));
		difficultyTableColumn.setCellValueFactory(new PropertyValueFactory<>("difficulty"));
		ingredientTableColumn.setCellValueFactory(new PropertyValueFactory<>("ingredients"));
		recipesTableView.setItems(FXCollections.observableArrayList(recipes));
	}

	private void initStock(){
		quantityTableColumn.setCellValueFactory(new PropertyValueFactory<>("count"));
		rawMaterialTableColumn.setCellValueFactory(new PropertyValueFactory<>("raw_material"));
		stockTableView.setItems(FXCollections.observableArrayList(stocks));
	}

	public void shopping() {
		Optional<Stock> keszlet = hu.alkfejl.dialog.Stock.show();
		if (keszlet.isPresent()) {
			ConnectSQL.getInstance().insertStock(keszlet.get());
			stocks.clear();
			stocks.addAll(ConnectSQL.getInstance().selectAllStock());
			stockTableView.setItems(FXCollections.observableArrayList(stocks));
		}
	}

	public void addIngredient() {
		Optional<RawMaterial> alapanyag = RawMaterialDialog.showAddDialog();
		if (alapanyag.isPresent()) {
			ConnectSQL.getInstance().insertRawMaterial(alapanyag.get());
			rawMaterials.clear();
			rawMaterials.addAll(ConnectSQL.getInstance().selectAllRawMaterial());
		}
	}

	public void removeIngredient() {
		RawMaterialDialog.showRemoveDialog(rawMaterials);
	}

	public void addRecipe() {
		Optional<Recipe> recept = RecipeDialog.show();
		if (recept.isPresent()) {
			ConnectSQL.getInstance().insertRecipe(recept.get());
			for (Stock hozzavalo : recept.get().getIngredients()) {
				ConnectSQL.getInstance().insertIngredient(hozzavalo);
			}
			recipes.clear();
			recipes.addAll(ConnectSQL.getInstance().selectAllRecipe());
			recipesTableView.setItems(FXCollections.observableArrayList(recipes));
		}
	}

	public void minIngredients() {
		MinRawMaterialsDialog.show();
	}

	public void addMenu() {
		Optional<Menu> menu = MenuDialog.show();
		if (menu.isPresent()) {
			Menu m = menu.get();
			int id = ConnectSQL.getInstance().insertMenu(m);
			for (Recipe recipe : m.getFoods()) {
				ConnectSQL.getInstance().insertSwitchRecipe(id, recipe.getId());
			}
		
			menus.add(m);
			menusTableView.setItems(FXCollections.observableArrayList(menus));
		}
	}

	public void closeCookBook() {
		Platform.exit();
	}
}
