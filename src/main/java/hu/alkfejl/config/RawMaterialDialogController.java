package hu.alkfejl.config;

import hu.alkfejl.model.Unit;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class RawMaterialDialogController implements Initializable {

	@FXML
	public TextField nameTextField;
	public ComboBox<Unit> unitComboBox;
	public TextField otherUnitTextField;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		unitComboBox.setItems(FXCollections.observableArrayList(Unit.values()));
	}
}
