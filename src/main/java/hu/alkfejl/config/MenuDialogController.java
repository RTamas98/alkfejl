package hu.alkfejl.config;


import hu.alkfejl.model.Recipe;
import hu.alkfejl.util.Regex;
import hu.alkfejl.util.Config;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class MenuDialogController implements Initializable {

	@FXML
	public TextField nameTextField;
	public Regex cookingTimeNumberField;
	public Regex enoughnessNumberField;
	public TableView<Recipe> recipesTableView;
	public TableColumn<Recipe, String> recipeNameTableColumn;
	public TableColumn<Recipe, String> recipeCategoryTableColumn;

	private final List<Recipe> recipes = MainController.getRecipes();
	public final List<Recipe> chosenrecipt = new ArrayList<>();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		recipeNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		recipeCategoryTableColumn.setCellValueFactory(new PropertyValueFactory<>("category"));
		recipesTableView.setItems(FXCollections.observableArrayList(chosenrecipt));
	}

	public void addNew(ActionEvent actionEvent) {
		final Dialog<Recipe> dialog = new Dialog<>();
		dialog.setTitle(Config.getExample().getResourceBundle().getString("application.name"));

		final ObservableList<Recipe> options = FXCollections.observableArrayList(recipes);
		final ComboBox<Recipe> receptComboBox = new ComboBox<>(options);
		dialog.getDialogPane().setContent(receptComboBox);

		final ButtonType buttonTypeOk = new ButtonType(Config.getExample().getResourceBundle().getString("dialog.add"), ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);

		dialog.setResultConverter(buttonType -> {
			if (buttonType == buttonTypeOk) {
				Recipe recipe = receptComboBox.getSelectionModel().getSelectedItem();
				chosenrecipt.add(recipe);
				return recipe;
			}
			return null;
		});
		Optional<Recipe> result = dialog.showAndWait();
		if (result.isPresent()) {
			System.out.println("Result: " + result.get());
			recipesTableView.setItems(FXCollections.observableArrayList(chosenrecipt));
		}
	}

	public void removeSelected(ActionEvent actionEvent) {
		Recipe selectedItem = recipesTableView.getSelectionModel().getSelectedItem();
		chosenrecipt.remove(selectedItem);
		recipesTableView.setItems(FXCollections.observableArrayList(chosenrecipt));
	}
}
