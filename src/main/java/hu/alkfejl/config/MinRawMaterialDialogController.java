package hu.alkfejl.config;


import hu.alkfejl.model.RawMaterial;
import hu.alkfejl.model.Stock;
import hu.alkfejl.util.ConnectSQL;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MinRawMaterialDialogController implements Initializable {

	@FXML
	public TableView<Stock> minRawMaterialsTableView;
	public TableColumn<Stock, String> rawMaterialTableColumn;
	public TableColumn<Stock, Integer> quantityTableColumn;

	private final List<RawMaterial> rawMaterials = ConnectSQL.getInstance().selectAllRawMaterial();
	private final List<Stock> minRawMaterials = new ArrayList<>();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		quantityTableColumn.setCellValueFactory(new PropertyValueFactory<>("count"));
		rawMaterialTableColumn.setCellValueFactory(new PropertyValueFactory<>("raw_material"));

		minRawMaterialsTableView.setEditable(true);
		quantityTableColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
		quantityTableColumn.setOnEditCommit(event -> {
			minRawMaterialsTableView.setItems(FXCollections.observableArrayList(minRawMaterials));
		});

		for (RawMaterial rawMaterial : rawMaterials) {
			Stock stock = new Stock.Builder()
					.setCount(0)
					.setRaw_material(rawMaterial)
					.build();
			minRawMaterials.add(stock);
		}
		minRawMaterialsTableView.setItems(FXCollections.observableArrayList(minRawMaterials));
	}
}
