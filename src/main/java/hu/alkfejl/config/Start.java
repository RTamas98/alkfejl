package hu.alkfejl.config;


import hu.alkfejl.Main;

import hu.alkfejl.util.Config;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ResourceBundle;

public class Start {

	private static Start instance = null;
	private final Stage stage;
	private Parent root;
	private MainController controller;
	private final ResourceBundle resourceBundle;

	private Start() {
		this.stage = Main.getStage();
		this.resourceBundle = Config.getExample().getResourceBundle();
	}

	public static Start getInstance() {
		return (instance == null) ? instance = new Start() : instance;
	}

	public void initScene() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
		loader.setResources(resourceBundle);
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		controller = loader.getController();
		stage.setScene(new Scene(getRoot()));
	}

	public Parent getRoot() {
		return root;
	}

	public MainController getController() {
		return controller;
	}
}
